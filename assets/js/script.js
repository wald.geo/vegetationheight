var RESOLUTIONS = [
  4000,
  3750,
  3500,
  3250,
  3000,
  2750,
  2500,
  2250,
  2000,
  1750,
  1500,
  1250,
  1000,
  750,
  650,
  500,
  250,
  100,
  50,
  20,
  10,
  5,
  2.5,
  2,
  1.5,
  1,
  0.5
];

var EXTENT = [2420000, 130000, 2900000, 1350000];
var PROJECTION = ol.proj.get("EPSG:2056");
PROJECTION.setExtent(EXTENT);

var MAX_SEARCH_RESULTS = 10;

var CENTER_X = 2660158;
var CENTER_Y = 1183640;
var INITIAL_ZOOM = 0;

var translations = {};

var reliefLayer;
var reliefLayerTransparency = 0.5;

var vegetationHeightLayer;
var vegetationHeightLayerTransparency = 0.5;

var mapX = CENTER_X;
var mapY = CENTER_Y;
var mapZoom = INITIAL_ZOOM;

var map;

function initialize() {
  fetchLanguages().then(function() {
    i18next.init(
      {
        lng: "de",
        debug: false,
        resources: translations
      },
      function(err, t) {
        // init set content
        updateContent();
      }
    );

    i18next.on("languageChanged", function() {
      updateContent();
    });

    initializeMap();

    readURL();

    document.body.addEventListener("click", handleBodyClick, true);
  });
}

function initializeMap() {
  var matrixIds = [];
  for (var i = 0; i < RESOLUTIONS.length; i++) {
    matrixIds.push(i);
  }

  var swisstopoLayer = new ol.layer.Tile({
    source: new ol.source.WMTS({
      url:
        "https://wmts0.geo.admin.ch/1.0.0/{Layer}/default/current/2056/{TileMatrix}/{TileCol}/{TileRow}.jpeg",
      tileGrid: new ol.tilegrid.WMTS({
        origin: [EXTENT[0], EXTENT[3]],
        resolutions: RESOLUTIONS,
        matrixIds: matrixIds
      }),
      projection: PROJECTION,
      layer: "ch.swisstopo.pixelkarte-grau",
      requestEncoding: "REST",
      attributions: [
        new ol.Attribution({
          html:
            '<a target="new" href="https://www.swisstopo.admin.ch/' +
            'internet/swisstopo/en/home.html">swisstopo</a>'
        })
      ]
    })
  });

  vegetationHeightLayer = new ol.layer.Tile({
    extent: EXTENT,
    source: new ol.source.TileWMS({
      url: "https://wms.geo.admin.ch/",
      params: {
        LAYERS: "ch.bafu.landesforstinventar-vegetationshoehenmodell",
        FORMAT: "image/png",
        TILED: true,
        VERSION: "1.1.1"
      },
      serverType: "mapserver",
      attributions: [
        new ol.Attribution({
          html: '<a target="new" href="https://www.lfi.ch/">LFI</a>'
        })
      ]
    })
  });

  reliefLayer = new ol.layer.Tile({
    extent: EXTENT,
    source: new ol.source.TileWMS({
      url: "https://wms.geo.admin.ch/",
      params: {
        LAYERS: "ch.bafu.landesforstinventar-vegetationshoehenmodell_relief",
        FORMAT: "image/png",
        TILED: true,
        VERSION: "1.1.1"
      },
      serverType: "mapserver"
    })
  });

  vegetationHeightLayer.setOpacity(vegetationHeightLayerTransparency);
  reliefLayer.setOpacity(reliefLayerTransparency);

  map = new ol.Map({
    layers: [swisstopoLayer, reliefLayer, vegetationHeightLayer],
    target: "map",
    view: new ol.View({
      center: [2598000, 1201000],
      projection: PROJECTION,
      maxResolution: 500,
      extent: EXTENT
    }),
    controls: ol.control
      .defaults({
        attributionOptions: {
          collapsible: false
        }
      })
      .extend([
        new ol.control.ScaleLine({
          target: document.getElementById("scale-line"),
          units: "metric"
        })
      ]),
    logo: false
  });

  map.on("moveend", function(evt) {
    pushURL();
  });

  map.on("click", function(evt) {
    var x0 = evt.coordinate[0] - 50;
    var x1 = evt.coordinate[0] + 50;
    var y0 = evt.coordinate[1] - 50;
    var y1 = evt.coordinate[1] + 50;
    var url =
      "https://wms.geo.admin.ch/?" +
      "SERVICE=WMS" +
      "&VERSION=1.3.0" +
      "&REQUEST=GetFeatureInfo" +
      "&FORMAT=image%2Fpng" +
      "&TRANSPARENT=true" +
      "&QUERY_LAYERS=ch.bafu.landesforstinventar-vegetationshoehenmodell_visual,ch.bafu.landesforstinventar-vegetationshoehenmodell_get_year" +
      "&LAYERS=ch.bafu.landesforstinventar-vegetationshoehenmodell_visual,ch.bafu.landesforstinventar-vegetationshoehenmodell_get_year" +
      "&FEATURE_COUNT=1" +
      "&INFO_FORMAT=text%2Fplain" +
      "&LANG=de" +
      "&I=50" +
      "&J=50" +
      "&CRS=EPSG%3A2056" +
      "&STYLES=" +
      "&WIDTH=101" +
      "&HEIGHT=101" +
      "&BBOX=" +
      x0 +
      "%2C" +
      y0 +
      "%2C" +
      x1 +
      "%2C" +
      y1;
    makeRequest(url, "GET").then(function(response) {
      var data = response.response;
      var heightLine = data.match(/Vegetationshöhe_m = '\d+\.\d+'/g);
      var dateLine = data.match(/Datenstand = '\d{4}-\d{2}-\d{2}'/g);
      if (!heightLine) {
        document.getElementById("info").classList.remove("hidden");
        document.getElementById("vegetation-height-label").classList.add("hidden");
        document.getElementById("vegetation-height").classList.add("hidden");
        document.getElementById("date-label").classList.add("hidden");
        document.getElementById("date").classList.add("hidden");
        document.getElementById("no-vegetation").classList.remove("hidden");
        return;
      }
      var height = parseFloat(heightLine[0].match(/\d+\.\d+/));
      var date = dateLine[0].match(/\d{4}-\d{2}-\d{2}/);
      document.getElementById("info").classList.remove("hidden");
      document.getElementById("vegetation-height-label").classList.remove("hidden");
      document.getElementById("vegetation-height").classList.remove("hidden");
      document.getElementById("date-label").classList.remove("hidden");
      document.getElementById("date").classList.remove("hidden");
      document.getElementById("no-vegetation").classList.add("hidden");
      document.getElementById("vegetation-height").innerText = "" + Math.round(height * 10) / 10 + "m";
      document.getElementById("date").innerText = date;
    });
  });
}

function hideVegetationHeight() {
  document.getElementById("info").classList.add("hidden");
}

/**
 * Read the URL search params and set the variables according to them.
 * This makes it possible to share the link to a specific view.
 * Possible params:
 * - mapX {Number} the X Coordinate of the map center
 * - mapY {Number} the YCoordinate of the map center
 * - zoom {Number} the zoom level of the map
 * - spezies {String} the latin name of the plant layer to be shown
 * - lang {String} the language, supporting de, fr, it and en
 */
function readURL() {
  var searchParams = new URLSearchParams(location.search);

  if (searchParams.has("mapX")) {
    var x = parseFloat(searchParams.get("mapX"));
    if (!Number.isNaN(x)) {
      mapX = x;
    }
  }

  if (searchParams.has("mapY")) {
    var y = parseFloat(searchParams.get("mapY"));
    if (!Number.isNaN(y)) {
      mapY = y;
    }
  }

  if (searchParams.has("zoom")) {
    var zoom = parseFloat(searchParams.get("zoom"));
    if (!Number.isNaN(zoom)) {
      mapZoom = zoom;
    }
  }

  if (searchParams.has("vegetationHeightTransparency")) {
    var transparency = parseFloat(searchParams.get("vegetationHeightTransparency"));
    if (!Number.isNaN(transparency) && transparency >= 0 && transparency <= 1) {
      vegetationHeightLayerTransparency = transparency;
      document.getElementById("vegetation-height-transparency").value = vegetationHeightLayerTransparency;
    }
  }

  if (searchParams.has("reliefTransparency")) {
    var transparency = parseFloat(searchParams.get("reliefTransparency"));
    if (!Number.isNaN(transparency) && transparency >= 0 && transparency <= 1) {
      reliefTransparency = transparency;
      document.getElementById("relief-transparency").value = reliefLayerTransparency;
    }
  }

  if (map && map.getView()) {
    var view = map.getView();
    view.setCenter([Math.round(mapX), Math.round(mapY)]);
    view.setZoom(Math.round(mapZoom * 100.0) / 100.0);
  }

  if (searchParams.has("lang")) {
    changeLanguage(searchParams.get("lang"));
  }
}

/**
 * Generate the URL search params for the current view and push it to the window history
 */
function pushURL() {
  var baseUrl = location.protocol + "//" + location.host + location.pathname;

  var searchParams = new URLSearchParams();

  mapX = map.getView().getCenter()[0];
  mapY = map.getView().getCenter()[1];
  mapZoom = map.getView().getZoom();

  searchParams.set("mapX", mapX);
  searchParams.set("mapY", mapY);
  if (mapZoom != undefined) {
    searchParams.set("zoom", mapZoom);
  }

  searchParams.set("vegetationHeightTransparency", vegetationHeightLayerTransparency);
  searchParams.set("reliefTransparency", reliefLayerTransparency);

  searchParams.set("lang", i18next.language);

  var parameter = "?" + searchParams.toString();

  var url = baseUrl + parameter;

  window.history.pushState({}, window.title, url);
}

/**
 * Make an asynchronous request and return a promise for this request
 * @param {String} url the url to make the request to
 * @param {String} method the HTTP request method to use for this request
 * @returns {Promise} a promise for the request
 */
function makeRequest(url, method) {
  // Create the XHR request
  var request = new XMLHttpRequest();

  // Return it as a Promise
  return new Promise(function(resolve, reject) {
    // Setup our listener to process compeleted requests
    request.onreadystatechange = function() {
      // Only run if the request is complete
      if (request.readyState !== 4) return;

      // Process the response
      if (request.status >= 200 && request.status < 300) {
        // If successful
        resolve(request);
      } else {
        // If failed
        reject({
          status: request.status,
          statusText: request.statusText
        });
      }
    };

    // Setup our HTTP request
    request.open(method || "GET", url, true);

    // Send the request
    request.send();
  });
}

function handleVegetationHeightTransparencyChange(event) {
  var transparency = event.target.value;

  vegetationHeightLayerTransparency = transparency;
  vegetationHeightLayer.setOpacity(transparency);

  pushURL();
}

function handleReliefTransparencyChange(event) {
  var transparency = event.target.value;

  reliefLayerTransparency = transparency;
  reliefLayer.setOpacity(transparency);

  pushURL();
}

/**
 * Search a location using the swisstopo location search service
 * @param {Event} event the input event of the location search bar
 */
function handleLocationSearch(event) {
  var searchQuery = event.target.value;

  if (!searchQuery) {
    hideSearchResults();
    return;
  }

  var requestURL =
    "https://api3.geo.admin.ch/rest/services/api/SearchServer?searchText=" +
    encodeURI(searchQuery) +
    "&type=locations&sr=2056&limit=" +
    MAX_SEARCH_RESULTS;

  makeRequest(requestURL).then(function(request) {
    var results = JSON.parse(request.response).results;
    displaySearchResults(results);
  });
}

/**
 * Display the location search results in a dropdown
 * @param results an array of the search results to be displayed
 */
function displaySearchResults(results) {
  var resultsElement = document.getElementById("search-results");
  resultsElement.innerHTML = "";

  if (!results) {
    hideSearchResults();
    return;
  }

  results.forEach(function(result) {
    var resultRow = document.createElement("div");
    resultRow.classList.add("result-row");
    resultRow.innerHTML = result.attrs.label;
    resultRow.setAttribute("onclick", "handleLocationClick(" + JSON.stringify(result) + ");");

    document.getElementById("search-results").appendChild(resultRow);
  });

  resultsElement.classList.remove("hidden");
}

/**
 * Hide the search results dropdown
 */
function hideSearchResults() {
  var resultsElement = document.getElementById("search-results");
  resultsElement.classList.add("hidden");
  resultsElement.innerHTML = "";
}

/**
 * Move the map to the clicked location and set an appropriate zoom level.
 * @param result the location to move to
 */
function handleLocationClick(result) {
  var view = map.getView();
  view.setZoom(Math.min(result.attrs.zoomlevel, 10)); // Cap zoomlevel because the api sometimes returns 2^32-1 as zoomlevel
  view.setCenter([result.attrs.y, result.attrs.x]);

  hideSearchResults();
}

/**
 * Hide search results dropdown if there is a click outside the location search element
 * @param {Event} event the click event used to determine the click target
 */
function handleBodyClick(event) {
  if (!(event.target.id === "location-search-input" || event.target.classList.contains("result-row"))) {
    hideSearchResults();
  }
}

function recenterMap() {
  var view = map.getView();
  view.setCenter([CENTER_X, CENTER_Y]);
  view.setZoom(INITIAL_ZOOM);
}

/**
 * Fetch the language JSON files
 */
function fetchLanguages() {
  var baseUrl = "assets/locales/";
  var languages = ["de", "fr", "it", "en"];

  return Promise.all(
    languages.map(function(language) {
      return makeRequest(baseUrl + language + ".json").then(function(request) {
        translations[language] = {};
        translations[language].translation = JSON.parse(request.response);
      });
    })
  );
}

/**
 * Update all content with the translated strings
 */
function updateContent() {
  document.getElementById("location-search-input").setAttribute("placeholder", i18next.t("searchLocation"));
  document.getElementById("transparency-label").innerText = i18next.t("transparency");
  document.getElementById("vegetation-height-transparency-label").innerText = i18next.t("vegetationHeight");
  document.getElementById("relief-transparency-label").innerText = i18next.t("relief");
  document.getElementById("vegetation-height-label").innerText = i18next.t("modeledVegetationHeight");
  document.getElementById("no-vegetation").innerText = i18next.t("noVegetation");
  document.getElementById("date-label").innerText = i18next.t("modelDate");
}

function changeLanguage(lng) {
  i18next.changeLanguage(lng);
  pushURL();
}

function centerOnCurrentLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(centerOnPosition);
  } else {
    alert("Geolocation is not supported by this browser.");
  }
}

function centerOnPosition(position) {
  var coords = proj4("EPSG:4326", "EPSG:2056", [position.coords.longitude, position.coords.latitude]);
  map.getView().setCenter(coords);
  map.getView().setZoom(4);
}

initialize();
